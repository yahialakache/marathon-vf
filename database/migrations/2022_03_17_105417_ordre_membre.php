<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class OrdreMembre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            
            $table->integer('ordre')->primary();
            $table->string('distance');

        });

        DB::table('order')->insert([
            ['ordre' => '1', 'distance' => '5'],
            ['ordre' => '2', 'distance' => '10'],
            ['ordre' => '3', 'distance' => '5'],
            ['ordre' => '4', 'distance' => '10'],
            ['ordre' => '5', 'distance' => '5'],
            ['ordre' => '6', 'distance' => '7.5'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
