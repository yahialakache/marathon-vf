<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Members extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->integer('idMember')->unique()->unsigned();
            $table->integer('idTeam')->unsigned();
            $table->integer('orderMember');

            $table->foreign('idMember')->references('id')->on('users');
            $table->foreign('idTeam')->references('id')->on('teams');
            $table->foreign('orderMember')->references('ordre')->on('order');
            $table->primary(['idMember','idTeam','orderMember']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
