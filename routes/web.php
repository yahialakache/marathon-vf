<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TeamsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});

Route::get('/team_create', [TeamsController::class, 'goToTeamCreateView'])->middleware(['auth'])->name('get_team_create_view');
Route::post('/team_create', [TeamsController::class, 'storeTeamCreatedByLeader'])->middleware(['auth'])->name('store_team_and_leader');

Route::get('/teams_settings', [TeamsController::class, 'goToTeamSettingsView'])->middleware(['auth'])->name('teams_settings');

Route::get('/teams_add_member', [TeamsController::class, 'goToAddMemberView'])->middleware(['auth'])->name('get_team_member_add_view');
Route::post('/teams_add_member', [TeamsController::class, 'storeTeamMemberByLeader'])->middleware(['auth'])->name('store_team_member');

Route::get('/join_team', [TeamsController::class, 'goToJoinTeamView'])->middleware(['auth'])->name('get_teams_to_join');
Route::post('/join_team', [TeamsController::class, 'storeTeamMemberByLeader'])->middleware(['auth'])->name('store_joining_member');



Route::get('/dashboard', function () {return view('dashboard');})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
