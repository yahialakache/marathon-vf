<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">

        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('store_team_and_member') }}">
            @csrf

           <!-- Team name -->
           <div>
                <x-label for="teamName" :value="__('Team Name')" />

                <x-input id="teamName" class="block mt-1 w-full" type="text" name="teamName" :value="old('teamName')" required autofocus />
            </div>
            
            <!-- Order Member  -->

            <div>
                <x-label for="order" :value="__('Choose your ordre')" />
                <select name="orderMember" id="order" class="block mt-1 w-full">

                    @foreach($order as $a)
                    <option value="{{$a->ordre}}" class="orderMember">{{$a->ordre}} : Distance covred  {{$a->distance}} </option>
                    

                    @endforeach
                </select>
            </div>
            <div class="add">

             </div>

            <div class="flex items-center justify-center mt-4">

                <x-button class="ml-4">
                    {{ __('Submit') }}
                </x-button>

            </div>      
            
                    
        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                count=0;
                $(".add_member").click(function(e){
                    e.preventDefault();
                    $(".add").append(`
                                <div>
                                    <x-label for="email" :value="__('Member ${count+1} E-mail')" />

                                    <x-input id="email" class="block mt-1 w-full" type="email" name="email${count+1}" :value="old('email${count+1}')" required autofocus />
                                </div>

                                <!-- Order Member  -->

                                <div>
                                    <x-label for="order" :value="__('Choose your ordre')" />
                                    <select name="orderMember${count+1}" id="order" class="block mt-1 w-full">

                                        @foreach($order as $a)
                                        <option value="{{$a->ordre}}" selected="true" >{{$a->ordre}} : Distance covred  {{$a->distance}} </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="flex items-center justify-end mt-4">
                    
                                </div>`);
                                count++;
                                if(count==5){
                                    $(".add_member").remove();
                                    count=0;
                                }



                })
            })

        </script>
        


    </x-auth-card>
</x-guest-layout>