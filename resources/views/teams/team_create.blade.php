@extends('layouts.app')
@section('content')
@include('teams.teams_side_bar')
    <div class="flex  text-left text-black  mt-16 ml-16 mb-0">
            <h1 class="text-4xl  grid  divide-black  "> Create a Team</h1>
    </div>
    <x-section-border />
<div class="flex item-center content-center place-content-center mt-28  ">

    <!-- Validation Errors -->
    <x-auth-validation-errors class="mb-4" :errors="$errors" />

<div class=" z-10 w-1/2 mt-20 ">
        <form method="POST" action="{{ route('store_team_and_leader') }}">
            @csrf

            <!-- Team name -->
            <div>
                <x-label for="teamName" :value="__('Team Name')" />

                <x-input id="teamName" style="color: black;" class="block mt-1 w-full text-xl" type="text" name="teamName" :value="old('teamName')" required autofocus />
            </div>

            <!-- Order Member  -->

            <div class="pt-4">
                <x-label for="order" :value="__('Choose your ordre')" />
                <select name="orderMember" id="order" class="block mt-1 w-full text-xl" style="color: black;">

                    @foreach($order as $a)

                    <option value="{{$a->ordre}}" class="orderMember">Place {{$a->ordre}} : Distance covred {{$a->distance}} km </option>


                    @endforeach
                </select>
            </div>


            <div class="flex items-center justify-center mt-4">

                <x-button class="ml-4">
                    {{ __('Create Team') }}
                </x-button>

            </div>


        </form>
    </div>


</div>
@endsection