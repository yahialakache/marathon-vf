<nav class="
relative
w-full
flex flex-row
items-center
justify-between
py-4
bg-gray-100
text-gray-500
hover:text-gray-700
focus:text-gray-700
shadow-lg
navbar navbar-expand-lg navbar-light
">
  <div class="container-fluid w-full flex flex-row  place-items-center justify-between px-6">
    
    <div class="collapse navbar-collapse flex flex-row items-center" id="navbarSupportedContent">
      
      <!-- Left links -->
      <ul class="navbar-nav flex flex-row pl-0 list-style-none mr-auto">
        <li class="nav-item px-11">
          <a class="nav-link text-xl bg-gradient-to-r hover:from-gray-500 hover:to-black hover:text-white rounded-xl p-3" href="{{route('get_team_create_view')}}">Create a Team</a>
        </li>
        <li class="nav-item pr-11">
          <a class="nav-link text-xl bg-gradient-to-r hover:from-gray-500 hover:to-black hover:text-white rounded-xl p-3" href="{{route('get_team_member_add_view')}}">Add Members</a>
        </li>
        <li class="nav-item pr-11">
          <a class="nav-link text-xl bg-gradient-to-r hover:from-gray-500 hover:to-black hover:text-white rounded-xl p-3" href="{{route('get_teams_to_join')}}">Join a Team</a>
        </li>
        <li class="nav-item pr-11">
          <a class="nav-link text-xl bg-gradient-to-r hover:from-gray-500 hover:to-black hover:text-white rounded-xl p-3" href="{{route('get_team_create_view')}}">Join Randomly</a>
        </li>
        <li class="nav-item pr-11">
          <a class="nav-link text-xl bg-gradient-to-r hover:from-gray-500 hover:to-black hover:text-white rounded-xl p-3" href="{{route('get_team_create_view')}}">View Your Team</a>
        </li>

      </ul>
      <!-- Left links -->
    </div>
    <!-- Collapsible wrapper -->
  </div>
</nav>