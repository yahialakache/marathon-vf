@extends('layouts.app')
@section('content')
@include('teams.teams_side_bar')

@if (session()->has('success'))

<div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
    <strong class="font-bold">🎉🎉🎉 BRAVOO 🎉🎉🎉</strong>
    <span class="block sm:inline"> 
    
        <li>Your Team Has been Created</li>
        
        
    </span>
    <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
        <svg class="fill-current h-6 w-6 text-green-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <title>Close</title>
            <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
        </svg>
    </span>
</div>
session()->forget('success')

@endif

<x-auth-validation-errors class="mb-4" :errors="$errors" />
<div class="flex flex-row items-center content-center ">

    <div class="flex flex-col w-full grid-cols-1 divide-y divide-gray-500 items-center">
        <div class="pt-2 w-2/3 z-10 hover:opacity-50 rounded-xl flex flex-col text-center items-center text-2xl text-black font-serif font-bold">
            <p>Create your team,<br> be a leader and get your trophy</p>
            <a href="{{route('get_team_create_view')}}"><img src="{{URL('images/create_team.png')}}" alt="Create Team"></a>
        </div>

        <div class="pt-8 w-2/3 z-10 hover:opacity-50 rounded-xl flex flex-col text-center items-center text-2xl text-black font-serif font-bold">
            <p>Join a random team <br> and show them what you're made of !</p>
            <a href=""><img src="{{URL('images/join_random.png')}}" class="" alt=""></a>
        </div>
    </div>
    <div class="flex flex-col w-full  grid-cols-1 divide-y divide-gray-500 items-center">
        <div class="w-2/3 z-10 hover:opacity-50 rounded-xl flex flex-col text-center items-center text-2xl text-black font-serif font-bold">
            <p>Add your team member <br> and build your strength for our event</p>
            <a href="{{route('get_team_member_add_view')}}"><img src="{{URL('images/addMember.png')}}" alt="add_Member"></a>
        </div>

        <div class="pt-8 w-2/3 z-10 hover:opacity-50 rounded-xl flex flex-col text-center items-center text-2xl text-black font-serif font-bold">
            <a href="{{route('get_teams_to_join')}}"><p>Collaborate with your friends,<br> and push your limits</p></a>
            <img src="{{URL('images/join_team.png')}}" alt="">
        </div>
    </div>

</div>
@endsection