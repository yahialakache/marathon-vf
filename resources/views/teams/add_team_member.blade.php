@extends('layouts.app')
@section('content')
@include('teams.teams_side_bar')

@if (session()->has('success2'))

<div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
    <strong class="font-bold">🎉🎉🎉 BRAVOO 🎉🎉🎉</strong>
    <span class="block sm:inline"> 
    
        <li>Your Member has been added</li>
        
        
    </span>
    <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
        <svg class="fill-current h-6 w-6 text-green-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <title>Close</title>
            <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
        </svg>
    </span>
</div>
session()->forget('success2')

@endif
<div class="flex  text-left text-black  mt-16 ml-16 mb-0">
    <h1 class="text-4xl  grid  divide-black  ">Add Member</h1>
</div>

<x-section-border />
<x-auth-validation-errors class="mb-4" :errors="$errors" />
<div class="flex item-center content-center place-content-center mt-28  ">
    
    <div class=" z-10 w-1/2 mt-20 ">
        <form method="POST" action="{{ route('store_team_member') }}">
            @csrf

            <!-- first Member -->
            <div>
                <x-label for="email" :value="__('Email Member')" />

                <x-input style="color: black;" id="email" class="block mt-1 w-full" type="text" name="email" :value="old('email')" required autofocus />
            </div>


            <!-- Order Member  -->

            <div class="pt-4">
                <x-label for="order" :value="__('Choose your ordre')" />
                <select name="orderMember" id="order" class="block mt-1 w-full text-xl" style="color: black;">

                    @foreach($order as $a)

                    <option value="{{$a->ordre}}" class="orderMember">Place {{$a->ordre}} : Distance covred {{$a->distance}} km </option>


                    @endforeach
                </select>
            </div>


            <div class="flex items-center justify-center mt-4">

                <x-button class="ml-4">
                    {{ __('Submit') }}
                </x-button>
                

            </div>


        </form>
    </div>
</div>

</div>
@endsection

