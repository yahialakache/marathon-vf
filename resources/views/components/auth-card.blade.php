<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100 text-xl">
    <div class="  z-10 w-32">
        {{ $logo }}
    </div>

    <div class="  mt-6 px-6 py-4 h-1/2 w-1/3 bg-white shadow-md overflow-hidden sm:rounded-lg z-10 text-xl">
        {{ $slot }}
    </div>
</div>
