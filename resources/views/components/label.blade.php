@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-medium text-sm text-black text-xl' ]) }}>
    {{ $value ?? $slot }}
</label>
