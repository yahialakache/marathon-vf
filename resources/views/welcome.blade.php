<!DOCTYPE html>
<html>

<head>
    <title>W3.CSS Template</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link href="https://unpkg.com/tailwindcss@^2.0/dist/tailwind.min.css" rel="stylesheet">

    <style>
        body,
        h1 {
            font-family: "Raleway", sans-serif
        }

        body,
        html {
            height: 100%
        }

        .bgimg {
            min-height: 100%;


        }

        .bgimg::before {
            content: "";
            background-image: url('https://lopinion.com/storage/articles/UWXBMcIof6NUXkgO9hOiTpU8eiAbPZibRS5mvTOb.jpg');
            background-size: cover;
            position: absolute;
            top: 0px;
            right: 0px;
            bottom: 0px;
            left: 0px;
            opacity: 0.5;
        }

       
    </style>
</head>

<body>

    <div class="bgimg w3-display-container w3-animate-opacity w3-text-white">
        <div class="w3-display-topleft w3-padding-large w3-xlarge">
            <img src="{{URL('images/logo5.png')}}" alt="logo" class="w-44">
        </div>

        <div class="w3-display-middle">
            @if (Route::has('login'))
            <div class="top-0 right-0 px-6 py-4 sm:block items-center">
                @auth
                <a href="{{ url('/dashboard') }}" class="pb-2 pr-4 pl-4 pt-2 border-2 ring-black hover:ring-white text-2xl text-black  hover:text-white dark:text-gray-500 no-underline   hover:from-gray-300 bg-gradient-to-r hover:to-black rounded-xl mr-4 font-mono ">Dashboard</a>
                @else
                <a href="{{ route('login') }}" class="pb-2 pr-4 pl-4 pt-2 border-2 ring-black hover:ring-white text-2xl text-black  hover:text-white dark:text-gray-500 no-underline   hover:from-gray-300 bg-gradient-to-r hover:to-black rounded-xl mr-4 font-mono ">Log in</a>

                @if (Route::has('register'))
                <a href="{{ route('register') }}" class="pb-2 pr-4 pl-4 pt-2 border-2 ring-black hover:ring-white text-2xl text-black hover:text-white dark:text-gray-500 no-underline  hover:from-gray-300 bg-gradient-to-r hover:to-black rounded-xl ml-4 font-mono ">Register</a>
                @endif
                @endauth
            </div>
            @endif
        </div>

    </div>

</body>

</html>