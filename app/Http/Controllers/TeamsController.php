<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use App\Models\Teams;
use App\Models\Members;
use App\Models\User;
use Exception;
use GrahamCampbell\ResultType\Success;
use Illuminate\Cache\Console\ForgetCommand;

class TeamsController extends Controller
{

    public function goToTeamSettingsView()
    {
        //
        return view('teams.teams_settings');
        
    }

    
    public function goToAddMemberView()
    {   

        //
        try
        {
            if (Teams::checkIfYouAreTeamAdmin(Auth::id())) {

                $teamId = Teams::getTeamIdByLeaderId(Auth::id());
                $availableorder=Members::availableOrders($teamId);
                //dd(sizeof($availableorder));
                if(sizeof($availableorder)==0){
                    return redirect()->route('teams_settings')->withErrors('Your team is full');
                }

                return view('teams.add_team_member',['order' => $availableorder]);
                
            }else{
                throw new Exception('You can\'t use this feature, Your not an admin to any team ');
    
            }

        }catch(Exception $exception){

            return redirect()->route('teams_settings')->withInput()->withErrors($exception->getMessage());


        }
        

    }

    public function goToTeamCreateView()
    {

       
        $order=Teams::getOrderTable();
        $teams=Teams::getTeamsTable();
            
        return view('teams.team_create',['order'=>$order,]);
    }

    public function goToJoinTeamView()
    {
       
        
        $teams=Teams::getAvailableTeamsToJoin();
        $availableorder=Teams::getAvailableOrdersToEachTeam($teams);
        //dd($availableorder);
            
        return view('teams.join_team',['order'=>$availableorder,'teams'=>$teams]);
    }



     
    public function storeTeamCreatedByLeader(Request $request)
    {
        //
        
        $request->validate([
            
            'teamName' => ['required', 'string', 'min:3', 'max:255', 'unique:teams'],
            'orderMember' => ['required'],
        ]);
    try{
        if(Teams::checkIfTheLeaderIsMemberinTeam(Auth::id())) 
        {
                Teams::addTeam([
                    'teamName' => $request->teamName,
                    'leader' => Auth::id(),
                ]);

                Members::addMember([
                    'idMember'=>Auth::id(),
                    'idTeam'=>Teams::getTeamIdByLeaderId(Auth::id()),
                    'orderMember'=>$request->orderMember,
        
                ]);
                //session()->put('success', 'You team have been Worked');
                   //View Code
                
                return redirect()->route('teams_settings')->withInput();
        }else
        {
            throw new Exception('You are a Member in annother team');
        }

    }catch(Exception $exception){
        return redirect()->route('get_team_create_view')->withInput()->withErrors($exception->getMessage());
    }

    // try{
    //     Members::addMember([
    //         'idMember'=>Auth::id(),
    //         'idTeam'=>Teams::getTeamIdByLeaderId(Auth::id()),
    //         'orderMember'=>$request->orderMember,

    //     ]);

    // }catch(Exception $exception){
    //     return redirect()->route('get_team_create_view')->withInput()->withErrors($exception->getMessage());


    // }
        
    }


    public function storeTeamMemberByLeader(Request $request)
    {
        //
        $request->validate([
            
            'email' => ['required', 'string', 'email', 'max:255'],
            
        ]);

        try
        {
            Members::addMember([
               
                'idMember'=>User::getUserIdByEmail($request->email),
                'idTeam'=>Teams::getTeamIdByLeaderId(Auth::id()),
                'orderMember'=>$request->orderMember,
    
            ]);

            //session()->put('success2', 'You Member has been added');
            return redirect()->route('goToAddMemberView')->withErrors('');
            
            


        }catch(Exception $exception){
            

            return redirect()->route('get_team_member_add_view')->withErrors('The member is not a user in our website OR he is member in annother team');


        }
        

    }

    // public function findActionToSubmitCreateTeamForm(Request $request){

    //     if($request->has('teamName'))
    //     {
    //         $this->storeTeam($request);

    //     }
    //     elseif($request->has('email'))
    //     {
    //         $this->storeTeamMemberByLeader($request);

    //     }
    // }
    

    
}
