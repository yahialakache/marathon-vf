<?php

namespace App\Models;

use ErrorException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\In;
use League\CommonMark\Extension\Table\Table;
use phpDocumentor\Reflection\Types\Boolean;
use PhpParser\Node\Expr\Print_;

use function PHPUnit\Framework\throwException;

class Teams extends Model
{
    use HasFactory;

    protected $fillable = [
        'teamName',
        'leader',
    ];

    public static function addTeam(array $table){
        
        DB::table('teams')->insert($table);

    }

    public static function deleteTeam(int $teamId){
        DB::table('teams')->delete($teamId);

    }

    public static function updateTeam(array $team, int $teamId){
        DB::table('teams')->where('id',$teamId)->update($team);

    }

    public static function getTeamIdByLeaderId(int $idUser):int
    {
       $array= DB::table('teams')->where('leader',$idUser)->get()->toArray();
        return $array[0]->id;
    }

    public static function getOrderTable():array
    {
        return DB::table('order')->get()->toArray();
        
    }

    public static function checkIfYouAreTeamAdmin(int $myId)
    {
        
        $ifExist=DB::table('teams')->where('leader',$myId)->get()->count();
        //dd($ifExist);
        if($ifExist==1)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }

    public static function getTeamNameByTeamId(int $teamId)
    {
        
         $array=DB::table('teams')->where('id',$teamId)->get()->toArray();
         return $array[0]->name;
        
    }

    public static function getTeamIdByTeamName(string $teamName)
    {
        
         $array=DB::table('teams')->where('name',$teamName)->get()->toArray();
         return $array[0]->id;
        
    }

    public static function checkTheTeamNumbe()
    {
        
       return DB::select('select count(*) from teams');
        
    }


    public static function getTeamsTable()
    {
        
         $array=DB::table('teams')->get()->toArray();
         return $array;
        
    }

    public static function getAvailableTeamsToJoin()
    {
        
         $array=DB::table('members')->join('teams','members.idTeam','=','teams.id')

                                    ->groupBy('members.idTeam')
                                    ->havingRaw('count(members.idMember) < ?',[6])
                                    ->get(['id','teamName'])
                                    ->toArray();
         //dd($array);
         return $array;
        
    }



    public static function getAvailableOrdersToEachTeam(array $teams)
    {
        $orderReservedTeams=[];
        //dd($teams);
        foreach($teams as $team){
            
            $orderReservedTeams[$team->id]=Members::getOrderReservedByTeamId($team->id);
        }
        $keys=array_keys($orderReservedTeams);
        $availableorder=[];
        //dd($keys);
        
        foreach($keys as $key){
            //dd($key);
            $availableorder[$key]=Members::availableOrders($key);
            
        }
        
        //dd($availableorder);
        return $availableorder;
        
    }


    public static function checkIfTheLeaderIsMemberinTeam(int $leaderId){
        $ifExist=DB::table('members')->where('idMember',$leaderId)->count();
        if($ifExist==1)
        {
            return false;
        }
        else
        {
            return true;
        }

    }




}
