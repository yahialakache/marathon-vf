<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Teams;
use Exception;

class Members extends Model
{
    use HasFactory;

    protected $fillable = [
        'idMember',
        'idTeam',
        'orderMember',
    ];

    public static function addMember(array $member){
        DB::table('members')->insert($member);
    

    }

    public static function deleteMember(int $memberId){
        DB::table('members')->where('idMember',$memberId)->delete();

    }

    public static function updateMember(array $member, int $idMember){
        DB::table('members')->where('idMember',$idMember)->update($member);

    }

  
    public static function getMemberByTeamName(string $teamName){
        
        $teamId=Teams::getTeamIdByTeamName($teamName);
        $array=DB::table('members')->where('idTeam',$teamId)->get('idMember')->toArray();
        return $array;
    }

    public static function getMembersByTeamId(int $teamId){
        
        $array=DB::table('members')->where('idTeam',$teamId)->get('idMember')->toArray();
        return $array;
    }

    public static function getTeamIdByMemberId(int $memberId){
        
        $teamId=DB::table('members')->where('idMember',$memberId)->get('idTeam');
       
        return $teamId;
    }

    public static function getOrderReservedByTeamId(int $teamId){
        
        $array=DB::table('members')->where('idTeam',$teamId)->get('orderMember')->toArray();
        return $array;
    }

    public static function getNumberOfMemberByTeamId(int $teamId){
        
        return DB::select('select count(*) from members where idTeam=:idTeam',['idTeam'=>$teamId]);
       
    }

    public static function getMemberOrderByMemberId(int $memberId){
        
        return DB::table('members')->where('idMember',$memberId)->get()->toArray()[0]->idMember;
       
    }

    public static function getMemberTableWithAllJoins(int $memberId){
        $teamId=Members::getTeamIdByMemberId($memberId);
        if($teamId->has('idTeam')){
            return DB::table('members')
                        ->join('teams','members.idTeam','=','teams.id')
                        ->join('users as memberInfo','members.idMember','=','memberInfo.id')
                        ->join('order','members.orderMember','=','order.ordre')
                        ->join('user as leaderInfo','teams.leader','=','leaderInfo.id')
                        ->where('idTeam',$teamId)
                        ->get('teams.teamName','leaderInfo.first_name as leaderFirstName','leaderInfo.last_name as leaderLastName',
                        'memberInfo.first_name as memberFirstName', 'memberInfo.last_name as memberLastName','ordre','distance')
                        ->toArray();
        }else
            throw new Exception('You are not in any Team');
        
       
    }
    function compare_objects($obj_a, $obj_b) {
        return $obj_a->id - $obj_b->id;
      }

    public static function availableOrders(int $teamId)
    {
        
        $order = Teams::getOrderTable();
        $members = Members::getOrderReservedByTeamId($teamId);
       

          foreach($members as $member){
            foreach($order as $ord){
                if($member->orderMember==$ord->ordre){
                    unset($order[array_search($ord, $order)]);
                }
            }
        }

        
        return $order;

    }



    




}
